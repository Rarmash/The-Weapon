# The Weapon
Создана за авторством @r4rm4sh в поддержку сервера Rebox Shit Force. Мой префикс — обычный слэш «/» и щепотка доверия.

## 🛠️ Установка
1. Клонируйте репозиторий:
```BASH
git clone https://github.com/Rarmash/The-Weapon.git
```
2. Смените директорию:
```BASH
cd The-Weapon
```
3. Установите зависимости:
```BASH
pip install -r requirements.txt
```
4. Запустите файл Python:
```BASH
python main.py
```
